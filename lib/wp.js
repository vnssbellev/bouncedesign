import axios from 'axios'

class WpApi {
  constructor(siteUrl) {
    this.apiBase = `${siteUrl}/wp-json`
  }

  posts(options) {
    const params = {
      page: 1,
      per_page: 99,
      ...options
    }
    return axios
      .get(`${this.apiBase}/wp/v2/posts`, { params })
      .then(json => {
        return { posts: json.data }
      })
      .catch(e => {
        return { error: e }
      })
  }

  projects(options) {
    const params = {
      page: 1,
      per_page: 99,
      ...options
    }
    return axios
      .get(`${this.apiBase}/wp/v2/projects-api`, { params })
      .then(json => {
        return { projects: json.data }
      })
      .catch(e => {
        return { error: e }
      })
  }

  pages(options) {
    const params = {
      page: 1,
      per_page: 30,
      ...options
    }
    return axios
      .get(`${this.apiBase}/wp/v2/pages`, { params })
      .then(json => {
        return { pages: json.data }
      })
      .catch(e => {
        return { error: e }
      })
  }

  team(options) {
    const params = {
      page: 1,
      per_page: 99,
      ...options
    }
    return axios
      .get(`${this.apiBase}/wp/v2/team-api`, { params })
      .then(json => {
        return { team: json.data }
      })
      .catch(e => {
        return { error: e }
      })
  }

  services(options) {
    const params = {
      page: 1,
      per_page: 99,
      ...options
    }
    return axios
      .get(`${this.apiBase}/wp/v2/services`, { params })
      .then(json => {
        return { services: json.data }
      })
      .catch(e => {
        return { error: e }
      })
  }

  authors(options) {
    const params = {
      page: 1,
      per_page: 20,
      ...options
    }
    return axios
      .get(`${this.apiBase}/wp/v2/users`, { params })
      .then(json => {
        return { users: json.data }
      })
      .catch(e => ({ error: e }))
  }

  siteData() {
    return axios
      .get(this.apiBase)
      .then(json => {
        const {
          name,
          description,
          url,
          ome,
          gmt_offset,
          timezone_string
        } = json.data
        return {
          site_data: {
            name,
            description,
            url,
            home,
            gmt_offset,
            timezone_string
          }
        }
      })
      .catch(e => ({ error: e }))
  }
}

const wp = new WpApi('http://bouncebeta.com/beta/bouncedesign/admin')

export default wp
