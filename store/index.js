import Vuex from 'vuex'
import wp from '~/lib/wp'

// Mutation Types
export const types = {
  SITE_DATA_UPDATE: 'SITE_DATA_UPDATE',
  POST_LIST_UPDATE: 'POST_LIST_UPDATE',
  PAGE_LIST_UPDATE: 'POST_LIST_UPDATE',
  PROJECT_LIST_UPDATE: 'PROJECT_LIST_UPDATE',
  TEAM_LIST_UPDATE: 'TEAM_LIST_UPDATE',
  AUTHORS_UPDATE: 'AUTHORS_UPDATE',
  CURRENT_POST_UPDATE: 'CURRENT_POST_UPDATE',
  CURRENT_PAGE_UPDATE: 'CURRENT_PAGE_UPDATE',
  CURRENT_PROJECT_UPDATE: 'CURRENT_PROJECT_UPDATE'
}

const createStore = () => {
  return new Vuex.Store({
    state: {
      site_data: {},
      post_list: [],
      page_list: [],
      project_list: [],
      team_list: [],
      authors: {},
      current_post: {},
      current_page: {},
      current_project: {}
    },
    mutations: {
      [types.SITE_DATA_UPDATE](state, payload) {
        state.site_data = { ...payload }
      },
      [types.POST_LIST_UPDATE](state, payload) {
        state.post_list = { ...payload }
      },
      [types.PAGE_LIST_UPDATE](state, payload) {
        state.page_list = { ...payload }
      },
      [types.PROJECT_LIST_UPDATE](state, payload) {
        state.project_list = { ...payload }
      },
      [types.TEAM_LIST_UPDATE](state, payload) {
        state.team_list = { ...payload }
      },
      [types.AUTHORS_UPDATE](state, payload) {
        state.authors_list = { ...payload }
      },
      [types.CURRENT_POST_UPDATE](state, payload) {
        state.current_post = { ...payload }
      },
      [types.CURRENT_PAGE_UPDATE](state, payload) {
        state.current_page = { ...payload }
      },
      [types.CURRENT_PROJECT_UPDATE](state, payload) {
        state.current_project = { ...payload }
      }
    },
    actions: {
      nuxtServerInit({ commit }) {
        const getSiteData = wp.siteData().then(res => {
          commit(types.SITE_DATA_UPDATE, res.site_data)
        })
        const getTeamData = wp.team().then(res => {
          commit(types.TEAM_LIST_UPDATE, res.team_list)
        })
        const getAuthors = wp.authors().then(res => {
          const authors = res.users.reduce((out, val) => {
            return {
              ...out,
              [val.id]: val
            }
          }, {})
          commit(types.AUTHORS_UPDATE, authors)
        })
        return Promise.all([getSiteData, getTeamData, getAuthors])
      }
    }
  })
}

export default createStore
