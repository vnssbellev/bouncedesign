import Vue from 'vue'
import VueScrollTo from 'vue-scrollto'

Vue.use(VueScrollTo, {
  duration: 500,
  easing: 'ease-in-out',
  offset: 0,
  x: false,
  y: true
})
